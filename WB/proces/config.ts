import { Config } from "../../src/types";

export const audio_source = 'PROCES.mp3';

// proces BPM = 120
// time between beats = 0.5s 
export const config: Config = {
    resolution: {
        x: 3840,
        y: 2160
    },
    fps: 60,
    durationInFrames: 16620,
    // durationInSeconds = 277
    // durationInFrames: 480,
    hexagonStepInS: 1,
    planet: {
        radius: 3840 / 1.5,
        position: {
            x: 3840 / 2,
            y: 2160 + 1400
        }
    },
    hex_type: {
        "1": { color: "#FFFFFF" },
        "2": { color: "#FFFFFF" },
        "3": { color: "#FFFFFF" },
        "4": { color: "#FFFFFF" },
        "5": { color: "#FFFFFF" }
    },
    subject: 'FloatingMan',

    gradients: [
        {
            id: 'darkest_blues',
            deg: 13 / 17,
            colors: ["#363c4c", "#1d2333", "#191c23",  "#203344", "#1e2637", "#2c2c2c" ]
        },
        
        {
            id: 'purpleish_brown',
            deg: 15 / 17,
            colors: ["#211f22", "#241c1a", "#2f252d",  "#443c47", "#171d1a" ]
        },
        
        {
            id: 'whitewood_brown',
            deg: 3 / 17,
            colors: ["#b1652c", "#b46b31", "#daac50",  "#e5d39f", "#e6dfc5" ]
        },
        
        {
            id: 'lightdark_brown',
            deg: 7 / 17,
            colors: ["#352725", "#3b2a23", "#a7663b",  "#b27f54", "#ab805d" ]
        },
        
        {
            id: 'turqoiese',
            deg: 10 / 17,
            colors: ["#24313a", "#2b3d46", "#23646d",  "#19737a", "#152d38" ]
        },
        
        {
            id: 'earthy_blues',
            deg: 9 / 17,
            type: 'radial',
            colors: ["#30373f", "#445159", "#4c483f",  "#79735d", "#645039","#30373f", "#445159", "#4c483f",  "#79735d", "#645039"  ]
        }
    ]
};