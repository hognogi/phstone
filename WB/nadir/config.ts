import { Config } from "../../src/types";

export const audio_source = 'NADIR.wav';

// proces BPM = 120
// time between beats = 0.5s 
export const config: Config = {
    resolution: {
        x: 3840,
        y: 2160
    },
    fps: 60,
    durationInFrames: 287 * 60,
    // durationInSeconds = 287
    // durationInFrames: 480,
    // nadir bpm = 100; 11 bpm = 80 porunca bpm = 110; 120 ... 1  100 = 100/120 = 1/1.2
    hexagonStepInS: 100/120,
    planet: {
        radius: 3840 / 1.5,
        position: {
            x: 3840 / 2,
            y: 2160 + 1400
        }
    },
    hex_type: {
        "1": { color: "#FFFFFF" },
        "2": { color: "#FFFFFF" },
        "3": { color: "#FFFFFF" },
        "4": { color: "#FFFFFF" },
        "5": { color: "#FFFFFF" }
    },
    subject: 'ManOnRock',

    gradients: [
        {
            id: 'sky_gradient',
            deg: 13 / 17,
            colors: ["#4a6e7a", "#0a1d27", "#322629",  "#203344", "#0a1d27", "#0a1d27" ]
        },
        
        {
            id: 'purpleish_brown',
            deg: 15 / 17,
            colors: ["#211f22", "#241c1a", "#2f252d",  "#443c47", "#171d1a" ]
        },
        
        {
            id: 'whitewood_brown',
            deg: 3 / 17,
            colors: ["#b1652c", "#b46b31", "#daac50",  "#e5d39f", "#e6dfc5" ]
        },
        
        {
            id: 'lightdark_brown',
            deg: 7 / 17,
            colors: ["#352725", "#3b2a23", "#a7663b",  "#b27f54", "#ab805d" ]
        },
        
        {
            id: 'turqoiese',
            deg: 10 / 17,
            colors: ["#24313a", "#2b3d46", "#23646d",  "#19737a", "#152d38" ]
        },
        
        {
            id: 'ground_gradient',
            deg: 9 / 17,
            type: 'radial',
            colors: ["#35262a", "#e3ba9d", "#c07b63", "#35262a",  "#79735d", "#8f7c78","#e6c4ac", "#9d887f", "#bd735e",  "#79735d", "#38292c"  ]
        },

        {
            id: 'sunflowers', 
            deg: 12/17,
            type: 'radial',
            colors: ['#dcc75c', '#f0db2a', '#a24607', '#e7e2cc', '#a8916f', '#be7554', '#f4c55b']
        }
    ]
};