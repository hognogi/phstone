import { PlanetGrid } from './PlanetGrid';
import { config, audio_source } from '../../WB/mount';
import { AbsoluteFill, Audio, staticFile, useCurrentFrame } from "remotion";
import { FloatingMan } from './FloatingMan';
import { ManOnRock } from './ManOnRock';
import { Gradients } from '../Components/Gradients';
import { useAudioData, visualizeAudio } from '@remotion/media-utils';
import { Babele } from './Babele';


const getSubjectAbove = () => {
  switch (config.subject) {
    case "ManOnRock": 
      return <ManOnRock isTop />
    case "Babele":
        return <Babele isTop/>
    default: 
      return <FloatingMan />
  }
}


const getSubjectBelow = () => {

  switch (config.subject) {

    case "ManOnRock":
      return <ManOnRock isTop={false} />

    case "Babele":
        return <Babele isTop={false}/>
    default: 
      return null;
  }
}

export const PlanetComposition = () => {

  const resolution = config.resolution!;

  const frame = useCurrentFrame();


  const sinBounce = Math.sin(frame / 1000) / 2 + 0.5;
  // const audioData = useAudioData(audio_source)!;
  

  

  return (
    <div>
      <Audio src={staticFile(audio_source)} />


      <svg height={resolution.y} width={resolution.x}>

        <filter id="blurMe"  >

          <feTurbulence type="turbulence"
            baseFrequency="1"
            numOctaves="2"
            result="turbulence" />

          <feDisplacementMap in2="turbulence"
            in="SourceGraphic" scale="50"
            xChannelSelector="R"
            yChannelSelector="B" />
        </filter>

        <filter id='noiseFilter'>
          <feTurbulence
            type='fractalNoise'
            baseFrequency='0.08'
            numOctaves='3'
            stitchTiles='stitch' />
        </filter>

        <defs>
          <Gradients />
        </defs>


        <g>
          {/* <g filter='url(#blurMe)' transform={`translate(${rootConfig.resolution.x / 2},${rootConfig.resolution.y})`} > */}
          <rect x="0" y="0" width={resolution.x} height={resolution.y} fill="url(#sky_gradient)"  />
          <image href={staticFile('starmap3.svg')}  width={resolution.x} height={5431} y={sinBounce * -1591} style={{"mixBlendMode":"lighten"}} />
          {/* <rect x="0" y="0" width={resolution.x} height={resolution.y} opacity={.1} fill="#FFFFFF" filter='url(#noiseFilter)' /> */}

          {/* <circle cx={planetConfig.position.x}
            cy={planetConfig.position.y}
            r={planetConfig.radius}
            stroke="black"
            strokeWidth="3"
            fill="red"
            fillOpacity={.3}
          /> */}

          {getSubjectBelow()}
          <g   mask="url(#myMask)">
            <rect x="0" y="0" width={resolution.x} height={resolution.y} fill="url(#ground_gradient)" />
            <image href={staticFile('noise.jpg')}  width={resolution.x} height={resolution.y} y={0}  style={{"mixBlendMode":"color-dodge"}}/>
          </g>
        {getSubjectAbove()}
        <mask id="myMask">

          <PlanetGrid />
        </mask>


          {/* <PlanetGrid /> */}
          {/* <text x={100} y={100}>{visualization_values}</text> */}
        </g>
        
      </svg>

    </div>
  );
};
