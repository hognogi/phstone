import { PlanetGrid } from './PlanetGrid';
import { audio_source, config } from '../../WB/mount';
import { useCurrentFrame, useVideoConfig, Img, staticFile, AbsoluteFill } from "remotion";

import { } from 'remotion';
import { useAudioData, visualizeAudio } from '@remotion/media-utils';


export const ManOnRock = ({isTop = true}) => {

  const resolution = config.resolution!;
  const planetConfig = config.planet!;



  const { fps } = useVideoConfig();

  const frame = useCurrentFrame();
  const audioData = useAudioData(staticFile(audio_source));




  const visualization_values = audioData && visualizeAudio({
    fps,
    frame,
    audioData,
    numberOfSamples: 2, // Use more samples to get a nicer visualisation
  });




  const sinBounce = Math.sin(frame / 20) / 2 + 0.5;

  // const audioData = useAudioData(audio_source)!;
  // console.log('AUDIO DATA', audioData);

  // const visualization_values = visualizeAudio({
  //   fps,
  //   frame,
  //   audioData,
  //   numberOfSamples: 12, // Use more samples to get a nicer visualisation
  // });

  const visualization_lower = Array.isArray(visualization_values) ? visualization_values[0] || 0 : 0;
  const blur_radius = (visualization_lower * 30).toFixed(2);


  if (isTop) {
    return (

      <>
            <image href={staticFile('rock_top.svg')}  width={`1500px`} height={resolution.y} y={`350px`} x={`500px`}   style={{ position: 'absolute', top: 800, left: 1500, filter: `blur(${blur_radius}px)`}} />

      </>
    )
  }

  return (<>
    <image href={staticFile('rock_bot.svg')}   width={`1500px`} height={resolution.y} y={`350px`} x={`500px`}    style={{ position: 'absolute', top: 800, left: 1500, filter: `blur(${blur_radius}px)`}} />

  </>
  );
};
