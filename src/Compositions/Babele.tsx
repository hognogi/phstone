import { PlanetGrid } from './PlanetGrid';
import { audio_source, config } from '../../WB/mount';
import { useCurrentFrame, useVideoConfig, Img, staticFile, AbsoluteFill } from "remotion";

import { } from 'remotion';
import { useAudioData, visualizeAudio } from '@remotion/media-utils';


export const Babele = ({isTop = true}) => {

  const resolution = config.resolution!;
  const planetConfig = config.planet!;



  const { fps } = useVideoConfig();

  const frame = useCurrentFrame();
  const audioData = useAudioData(staticFile(audio_source));




  const visualization_values = audioData && visualizeAudio({
    fps,
    frame,
    audioData,
    numberOfSamples: 2, // Use more samples to get a nicer visualisation
  });




  const sinBounce = Math.sin(frame / 20) / 2 + 0.5;

  // const audioData = useAudioData(audio_source)!;
  // console.log('AUDIO DATA', audioData);

  // const visualization_values = visualizeAudio({
  //   fps,
  //   frame,
  //   audioData,
  //   numberOfSamples: 12, // Use more samples to get a nicer visualisation
  // });

  const visualization_lower = Array.isArray(visualization_values) ? visualization_values[0] || 0 : 0;
  const blur_radius = (visualization_lower * 30).toFixed(2);

  /**
   * Bounce offsets
   * @type {number[]}
   */
  const bo = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    .map( n => Math.PI / 9 * n * 80)
    .map( n => Math.sin( (frame + n) / 80  )  / 2 + 0.5  )
    .map( n => n * 100);

  const opacity = .6;

  if (isTop) {
    return (

      <>
            <image href={staticFile('zidar/baba1.svg')}  width={`1500px`} height={resolution.y} y={`${250 + bo[0]}px`} x={`500px`}   style={{ position: 'absolute',  opacity, filter: `blur(${blur_radius}px)`, clipPath: 'ellipse(42% 33% at 48% 28%)'} } />
            {/* <image href={staticFile('zidar/baba2.svg')}  width={`1500px`} height={resolution.y} y={`${550 + bo[1]}px`} x={`1200px`}   style={{ position: 'absolute',  opacity, filter: `blur(${blur_radius}px)`, clipPath: 'ellipse(42% 33% at 48% 28%)'}} />
            <image href={staticFile('zidar/baba3.svg')}  width={`1500px`} height={resolution.y} y={`${450 + bo[2]}px`} x={`2700px`}   style={{ position: 'absolute',  opacity, filter: `blur(${blur_radius}px)`, clipPath: 'ellipse(44% 33% at 75% 28%)'}} />
            <image href={staticFile('zidar/baba4.svg')}  width={`1500px`} height={resolution.y} y={`${50 + bo[3]}px`} x={`2000px`}   style={{ position: 'absolute',  opacity, filter: `blur(${blur_radius}px)`, clipPath: 'ellipse(41% 33% at 48% 9%)'}} />
            <image href={staticFile('zidar/baba5.svg')}  width={`1500px`} height={resolution.y} y={`${50 + bo[4]}px`} x={`-100px`}   style={{ position: 'absolute',  opacity, filter: `blur(${blur_radius}px)`, clipPath: 'ellipse(49% 30% at 26% 28%)'}} /> */}
            {/* <image href={staticFile('zidar/baba2.svg')}  width={`1500px`} height={resolution.y} y={`${650 + bo[5]}px`} x={`2300px`}   style={{ position: 'absolute', filter: `blur(${blur_radius}px)`, clipPath: 'ellipse(42% 33% at 73% 28%)'}} />
            <image href={staticFile('zidar/baba3.svg')}  width={`1500px`} height={resolution.y} y={`${150 + bo[6]}px`} x={`900px`}   style={{ position: 'absolute', filter: `blur(${blur_radius}px)`, clipPath: 'ellipse(48% 31% at 47% 13%)'}} />
            <image href={staticFile('zidar/baba4.svg')}  width={`1500px`} height={resolution.y} y={`${50 + bo[7]}px`} x={`1500px`}   style={{ position: 'absolute', filter: `blur(${blur_radius}px)`, clipPath: 'ellipse(42% 33% at 52% 28%)'}} />
            <image href={staticFile('zidar/baba5.svg')}  width={`1500px`} height={resolution.y} y={`${1050 + bo[8]}px`} x={`100px`}   style={{ position: 'absolute', filter: `blur(${blur_radius}px)`, clipPath: 'ellipse(42% 33% at 21% 12%)'}} /> */}

      </>
    )
  }

  return (<>
    <image href={staticFile('zidar/baba1.svg')}  width={`1500px`} height={resolution.y} y={`${250 + bo[0]}px`} x={`500px`}   style={{ position: 'absolute',  opacity, filter: `blur(${blur_radius}px)`}}/>
    {/* <image href={staticFile('zidar/baba2.svg')}  width={`1500px`} height={resolution.y} y={`${550 + bo[1]}px`} x={`1200px`}   style={{ position: 'absolute',  opacity, filter: `blur(${blur_radius}px)`}} />
    <image href={staticFile('zidar/baba3.svg')}  width={`1500px`} height={resolution.y} y={`${450 + bo[2]}px`} x={`2700px`}   style={{ position: 'absolute',  opacity, filter: `blur(${blur_radius}px)`}}/>
    <image href={staticFile('zidar/baba4.svg')}  width={`1500px`} height={resolution.y} y={`${50 + bo[3]}px`} x={`2000px`}   style={{ position: 'absolute',  opacity, filter: `blur(${blur_radius}px)`}} />
    <image href={staticFile('zidar/baba5.svg')}  width={`1500px`} height={resolution.y} y={`${50 + bo[4]}px`} x={`-100px`}   style={{ position: 'absolute',  opacity, filter: `blur(${blur_radius}px)`}} /> */}
    {/* <image href={staticFile('zidar/baba2.svg')}  width={`1500px`} height={resolution.y} y={`${650 + bo[5]}px`} x={`2300px`}   style={{ position: 'absolute', filter: `blur(${blur_radius}px)`}} />
    <image href={staticFile('zidar/baba3.svg')}  width={`1500px`} height={resolution.y} y={`${150 + bo[6]}px`} x={`900px`}   style={{ position: 'absolute', filter: `blur(${blur_radius}px)`}} />
    <image href={staticFile('zidar/baba4.svg')}  width={`1500px`} height={resolution.y} y={`${50 + bo[7]}px`} x={`1500px`}   style={{ position: 'absolute', filter: `blur(${blur_radius}px)`}} />
    <image href={staticFile('zidar/baba5.svg')}  width={`1500px`} height={resolution.y} y={`${1050 + bo[8]}px`} x={`100px`}   style={{ position: 'absolute', filter: `blur(${blur_radius}px)`}} /> */}

  </>
  );
};
