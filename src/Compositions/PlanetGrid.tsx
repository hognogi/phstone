
import { PlanetConfig_Neutral, Vector2D, Vector2D_Neutral } from '../types';
import { Hexagon_Cart, Vector2D_Cart } from '../Tools/Math';
import { HexagonGrid } from '../Tools/DataStructures/HexagonGrid';
import { config } from '../../WB/mount';
import { cellMap as rawCellMap } from '../../WB/mount';
import { assert } from '../Tools/utils';


import { staticFile } from "remotion";

import { useAudioData, visualizeAudio } from '@remotion/media-utils';

import { audio_source } from '../../WB/mount';
import { useCurrentFrame, useVideoConfig } from 'remotion';


interface MapCell {

  coords: string,
  type: string,
  elapsed: string | null,
  next: string | null
}

const cellMap = (rawCellMap as unknown) as Record<string, MapCell>;


// "-14:0": {
//   "coords": "-14:0",
//   "type": "4",
//   "elapsed": null,
//   "next": null
// },

function hyperbolicMap(frame: number, vertex: Vector2D, origin: Vector2D): Vector2D {

  const fOfx = (x: number): number => Math.pow(x, 1 / 6) * 550;

  const dist = Vector2D_Cart.distBetween(vertex, origin);

  if (dist === 0) return Vector2D_Neutral();

  const dist_prime = fOfx(dist);

  const magnitude = dist_prime / dist;
  const vector_from_0_0 = Vector2D_Cart.sub(vertex, origin);
  const scaled_from_0_0 = Vector2D_Cart.timesScalar(vector_from_0_0, magnitude);
  return Vector2D_Cart.sum(scaled_from_0_0, origin); // back to origin




}


export const PlanetGrid = () => {

  const frame = useCurrentFrame();


  const planet_config = config.planet || PlanetConfig_Neutral();

  if (planet_config.radius === 0) return null;



  const HEX_SIDE = planet_config.radius / 4.5;
  const HEX_HEIGHT = Math.sqrt(3) * HEX_SIDE;

  // const HEX_NUMBER = (frame * config.pixelPerFrame!) / (HEX_HEIGHT * 4);

  const FRAMES_PER_HEXAGON = config.fps! * config.hexagonStepInS!;

  const OFFSET_IN_STEPS = frame / FRAMES_PER_HEXAGON;
  const OFFSET_IN_STEPS_INT = Math.floor(OFFSET_IN_STEPS);
  const NEXT_STEP_PROGRESS = OFFSET_IN_STEPS - OFFSET_IN_STEPS_INT;

  let cell_map_runner: string | null = "0:0";

  for (let i = 0; i < OFFSET_IN_STEPS_INT; i++) {
    cell_map_runner = cellMap[cell_map_runner].next!;
  }

  const CURRENT_HEXAGON = cellMap[cell_map_runner];

  // const NEXT = CURRENT_HEXAGON.next && Object.fromEntries(
  //   CURRENT_HEXAGON.next.split(':')
  //   .map(x => parseInt(x))
  //   .map((x, i) => ([['x', 'y'][i], x]))
  // ) || {x: 0, y: 0} as Vector2D;



  const current_coords_vector = HexagonGrid.vector2DFromHash(CURRENT_HEXAGON.coords);

  const next_coords_vector = HexagonGrid.vector2DFromHash(CURRENT_HEXAGON.next!);

  const COORD_DELTA = Vector2D_Cart.sub(next_coords_vector, current_coords_vector);

  const offset = {
    x: 0 ,
    // x: current_coords_vector.x * HEX_HEIGHT + (COORD_DELTA.x * NEXT_STEP_PROGRESS * HEX_HEIGHT * 2),
    y: current_coords_vector.y * HEX_HEIGHT + (COORD_DELTA.y * NEXT_STEP_PROGRESS * HEX_HEIGHT / 2),
  }

  // console.log(`${offset.x}:${offset.y}`);


  // const offset = OFFSET_IN_HEXAGONS > 4 && OFFSET_IN_HEXAGONS < 8 
  //   ? {y: 0, x: OFFSET_IN_HEXAGONS * HEX_HEIGHT}
  //   : {x: 0, y: OFFSET_IN_HEXAGONS * HEX_HEIGHT}

  // const remainder = Math.floor(HEX_NUMBER) % 3;


  const { fps } = useVideoConfig();

  const center_hexagon = Hexagon_Cart.fromCenterAndSide(
    // Vector2D_Cart.sum(planet_config.position, {x: 0, y: 0}),
    Vector2D_Cart.sum(planet_config.position, offset),
    // planet_config.position,
    HEX_SIDE
  );

  const hexagon_grid = HexagonGrid.fromCenterHexagon(center_hexagon);
  // hexagon_grid.solveLevel1();
  // hexagon_grid.solveLevel2();
  hexagon_grid.solveUntilOutOfBounds(

    current_coords_vector
  );


  const hexagon_grid_list = hexagon_grid.toHexagonCartArray()
  // .map(hex => hex.hyperbolicTransform())


  // const audioData = useAudioData(audio_source)!;
  const audioData = useAudioData(staticFile(audio_source));




  const visualization_values = audioData && visualizeAudio({
    fps,
    frame,
    audioData,
    numberOfSamples: 32, // Use more samples to get a nicer visualisation
  });



  const hexagon_grid_list_mapped = hexagon_grid_list.map(
    hex => ({
      hex: hex.hexagon_cart.toDistortedPoly((vertex) => hyperbolicMap(frame, vertex, planet_config.position)),
      coords: hex.coords,
      type: cellMap[hex.coords]?.type || '1'
    }),
  );



  
  const hexToSVG = ({ hex, coords }) => {
    const type = cellMap[coords]?.type || '1';
    const bgColor = config.hex_type[type]?.color || '#000000';
    const coords_vector = HexagonGrid.vector2DFromHash(coords);
    const dist_to_current_coords = Vector2D_Cart.magnitude(
      Vector2D_Cart.sub(
        coords_vector,
        current_coords_vector
      ));
    
    // max vector aprox {-17:65} -> max distance aprox: 67.18
    
    const current_sample_index = Math.floor( (dist_to_current_coords / 68) * 8 );
    const current_sample = visualization_values && visualization_values[current_sample_index]! || 1;

    if (type === '1'  && current_sample > .2) {
      hex.toTranslate({x: 0, y: -300 * (current_sample ** 2)});
    }


    if ((type === '2' || type === '4')) {
    // if (type === '2') {
    return null;
    }
    // hex.scaleToOrigin(0.85 + sinBounce * .15);
    // hex.scaleToOrigin(0.85 + current_sample * .12);
    //  hex.scaleToOrigin(0.95);
    // hex.toTranslate({x: 0, y: 100 * sinBounce});
    // const translatedHex = hex.transalte
    return (
      <g data-coords={coords}>
        {/* <path d={hex.toSVGPathD()} fill={bgColor} stroke={'#00000000'} strokeWidth="10" /> */}
        <path d={hex.toSVGPathD()} fill={'#fff'} stroke={'#00000000'} strokeWidth="10" />
        {/* <text x={hex.origin.x} y={hex.origin.y}  filter="url(#f2)"  fontSize={60}> {type} </text> */}
      </g>
    )
  };




  const hexes_to_translate = hexagon_grid_list_mapped.filter(({type}) => type === '1').map(hexToSVG)
  const hexes_non_translated = hexagon_grid_list_mapped.filter(({type}) => type !== '1').map(hexToSVG);
  
  

  // <g transform={`translate(${-context.root?.resolution.x!/2}, ${-context.root?.resolution.y!})`}>{drawHyperbola(frame)}</g>

  // <g transform={`translate(${-context.root?.resolution.x!/2}, ${-context.root?.resolution.y!})`}>{getPoints()}</g>
  // <g transform={`translate(${-context.root?.resolution.x!/2}, ${-context.root?.resolution.y!})`}>{getPoints(true)}</g>






  return (
    <>
      


      <g>
      <path d={center_hexagon.toSVGPathD()} stroke="black" strokeWidth="3" />

        {hexes_non_translated}
        {hexes_to_translate}
        </g>
    </>
  );
};
