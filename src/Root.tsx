import { Composition, useCurrentFrame, useVideoConfig } from 'remotion';
import { config } from '../WB/mount';


// const ROOT_CONFIG: RootConfig = {
//   resolution: {
//     x: 3840,
//     y: 2160
//   }
// };


import { PlanetComposition } from './Compositions/Planet';
import './style.css';
// import { Context } from './types'

const fps = config.fps!;
const durationInFrames = config.durationInFrames!;





export const RemotionRoot: React.FC = () => {
  const AUDIO_START = 0; // Seconds
  const { x: w, y: h } = config.resolution!;

  // const [config, setConfig] = useState({
  //   root: ROOT_CONFIG,
  //   appendConfig: (key: string, obj: any) => setConfig({ ...config, [key]: obj })
  // });


  return (<>
      <Composition
        id="Planet"
        component={PlanetComposition}
        durationInFrames={durationInFrames}
        fps={fps}
        width={w}
        height={h}
        defaultProps={{
          audioOffsetInFrames: Math.round(AUDIO_START * fps)
        }}
      />

  </>);
};

