import { useCurrentFrame } from 'remotion';
import { config } from '../../WB/mount';

export const Gradients = () => {

  const frame = useCurrentFrame();

  const sinBounce = Math.sin(frame / 100) / 2 + 0.5;



  return <>

    {config.gradients.map(gradient => {



      if (gradient.type === 'radial') {
        return <radialGradient id={gradient.id} cx="50%" cy="100%" r="100%" fx="50%" fy="100%">
          
          {gradient.colors.map((color, i) => {
            // const offset = ((i + (frame/40)) % gradient.colors.length) / gradient.colors.length;

            const offset = (i/gradient.colors.length ) + ( .6 * sinBounce);


            return <stop offset={offset} stop-color={color} />
          })}
        </radialGradient>
      }

      return <linearGradient id={gradient.id} x1="0" x2="0" y1="0" y2="1">
          {gradient.colors.map((color, i) => {
            const offset = ((i + (frame/40)) % gradient.colors.length) / gradient.colors.length;

            return <stop offset={i/gradient.colors.length} stop-color={color} />
          })}1
      </linearGradient >

    })}


  </>
}