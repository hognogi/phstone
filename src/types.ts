
export interface AppendConfigFunction {
	(key:string, a:any): void;
}

export interface Vector2D {
	x: number;
	y: number;
}

export const Vector2D_Neutral:() => Vector2D = () => ({x: 0, y: 0});

// export interface RootConfig {
// 	resolution: Vector2D 
// }


export const RootConfig_Neutral: () => RootConfig = () => ({
	resolution: Vector2D_Neutral()
});

export interface PlanetConfig {
	radius: number;
	position: Vector2D;
};

export const PlanetConfig_Neutral: () => PlanetConfig = () => ({
	radius: 0,
	position: Vector2D_Neutral()
});

export interface PlanetGridConfig {
	march_ratio: number
};


export interface Config {
  resolution?: Vector2D,
  fps?: number,
  durationInFrames?: number,
  hexagonStepInS?: number,
  planet?: PlanetConfig,
  hex_type: any,
	gradients: any,
	subject?: string
}



// export interface Context {
// 	appendConfig: AppendConfigFunction;
// 	root?: RootConfig;
// 	planet?: PlanetConfig;
// 	grid?: PlanetGridConfig
// }

