import { Vector2D, Vector2D_Neutral } from "../types";


export class Vector2D_Cart {
	public static sum(...args: Vector2D[]): Vector2D {
		const neutral = Vector2D_Neutral();

		return args.reduce((acc, crt) => ({
			x: acc.x + crt.x,
			y: acc.y + crt.y
		}), neutral);
	}

	public static sub(...args: Vector2D[]): Vector2D {

		const [head, ...tail] = args;

		return tail.reduce((acc, crt) => ({
			x: acc.x - crt.x,
			y: acc.y - crt.y
		}), head);
	}

	public static magnitude(v: Vector2D): number {
		return Math.sqrt(v.x ** 2 + v.y ** 2);
	}

	public static timesScalar(v: Vector2D, scalar: number): Vector2D {
		return {
			x: v.x * scalar,
			y: v.y * scalar
		}
	}

	public static distBetween(v1: Vector2D, v2: Vector2D) {
		return Math.sqrt((v2.x - v1.x) ** 2 + (v2.y - v1.y) ** 2);
	}
}


export class Segment_Cart {
	public A: Vector2D = Vector2D_Neutral();
	public B: Vector2D = Vector2D_Neutral();

	constructor(a: Vector2D, b: Vector2D) {
		this.A = a;
		this.B = b;
	}

	public getCenter(): Vector2D {
		return {
			x: (this.A.x + this.B.x) / 2,
			y: (this.A.y + this.B.y) / 2,
		}
	}

	public static fromVertices(a: Vector2D, b: Vector2D): Segment_Cart {
		return new Segment_Cart(a, b);
	}
}

export class Polygon_Cart {
  public vertices: Vector2D[] = [];
  public origin: Vector2D = Vector2D_Neutral()

  public scaleToOrigin(scalar: number) {
    // foreach vertice; (vertice - origin ) * scalar + origin

    this.vertices = this.vertices.map(vert => {
      let vert_prime = {...vert};
      vert_prime = Vector2D_Cart.sub(vert_prime, this.origin);
      vert_prime = Vector2D_Cart.timesScalar(vert_prime, scalar);
      vert_prime = Vector2D_Cart.sum(vert_prime, this.origin);
      return vert_prime;
    });

    return this;
  }

  public toTranslate(v: Vector2D) {

    this.origin = Vector2D_Cart.sum(this.origin, v);
    this.vertices = this.vertices.map( vertex => Vector2D_Cart.sum(vertex, v) );

  }

  public toSVGPathD(): string {

    if(this.vertices.length <= 2 ) {
      throw new Error("pls implement short polys");
    }

		const verts = this.vertices.map(v => ({ x: v.x.toPrecision(3), y: v.y.toPrecision(3) }));
    const head = verts[0];
    const last = verts.at(-1)!;
    const body = verts.slice(1, -1);

		return `M${head.x} ${head.y}` +
			// `L${B.x} ${B.y}` +
			body.map(vertex => `L${vertex.x} ${vertex.y}`).reduce((acc, crt) => acc + crt) +
			`L${last.x} ${last.y} Z`
	}
}

export class Hexagon_Cart {
	public center: Vector2D = Vector2D_Neutral();
	public side = 0;
	public vertices: Vector2D[] = [];


	private solveVertexesFromCenterAndSide() {
		this.vertices = [
			Vector2D_Cart.sum(this.center, { x: this.side, y: 0 }),  // Rad = 0
			Vector2D_Cart.sum(this.center, { x: this.side / 2, y: -1 * this.side * Math.sqrt(3) / 2 }), // Rad = π/3
			Vector2D_Cart.sum(this.center, { x: -1 * this.side / 2, y: -1 * this.side * Math.sqrt(3) / 2 }), // Rad = 2π/3
			Vector2D_Cart.sum(this.center, { x: -1 * this.side, y: 0 }),  // Rad = π
			Vector2D_Cart.sum(this.center, { x: -1 * this.side / 2, y: this.side * Math.sqrt(3) / 2 }), // Rad = 4π/3
			Vector2D_Cart.sum(this.center, { x: this.side / 2, y: this.side * Math.sqrt(3) / 2 }), // Rad = 5π/3
		]
	}

	public static fromCenterAndSide(center: Vector2D, side: number): Hexagon_Cart {
		const hex = new Hexagon_Cart();
		hex.center = center;
		hex.side = side;
		hex.solveVertexesFromCenterAndSide();
		return hex;
	}

	public toSVGPathD(): string {
		const [A, B, C, D, E, F] = this.vertices.map(v => ({ x: v.x.toPrecision(3), y: v.y.toPrecision(3) }));

		return `M${A.x} ${A.y}` +
			`L${B.x} ${B.y}` +
			`L${C.x} ${C.y}` +
			`L${D.x} ${D.y}` +
			`L${E.x} ${E.y}` +
			`L${F.x} ${F.y}` +
			`L${A.x} ${A.y} Z`
	}

	public getSides(): Segment_Cart[] {
		const [A, B, C, D, E, F] = this.vertices;

		return [
			Segment_Cart.fromVertices(A, B),
			Segment_Cart.fromVertices(B, C),
			Segment_Cart.fromVertices(C, D),
			Segment_Cart.fromVertices(D, E),
			Segment_Cart.fromVertices(E, F),
			Segment_Cart.fromVertices(F, A),
		];
	}

	public getNeighboursCenters(): Array<Vector2D> {
		const side = this.side;

		return [
			// [0,  pi/3]
			{
				x: side * 1.5,
				y: side * -Math.sqrt(3) / 2 
			},
			// [pi/3, 2pi/3] 
			{
				x: 0,
				y: side * -Math.sqrt(3) 
			},
			// [2pi/3, pi] 
			{
				x: side * -1.5,
				y: side * -Math.sqrt(3) / 2
			},
			// [pi, 4pi/3] 
			{
				x: side * -1.5,
				y: side * Math.sqrt(3) / 2
			},
			// [4pi/3, 5pi/3] 
			{
				x: 0,
				y: side * Math.sqrt(3)
			},
			// [5pi/3, 0] 
			{
				x: side * 1.5,
				y: side * Math.sqrt(3) / 2
			}
		];
	};
	
  public toDistortedPoly(map: (vertex: Vector2D) => Vector2D): Polygon_Cart {
    
    //return this.vertices.map(map);
    const new_poly = new Polygon_Cart();
    new_poly.vertices = this.vertices.map(map);
    new_poly.origin = map(this.center); 
    return new_poly;
  }

}


export class Circle_Cart {
	public C: Vector2D = Vector2D_Neutral();
	public r = 0;

	// TODO: deprecate?
	public static fromCenterAndRadius(C: Vector2D, r: number): Circle_Cart {
		const new_c = new Circle_Cart();
		new_c.C = C;
		new_c.r = r;
		return new_c;
	}

	// TODO: deprecate
	public static fromTangentAndPerpendicularCircle(tangent: Vector2D, outer_circle: Circle_Cart): Circle_Cart {

		// A, b, c sides of the triangle formed by the centers of the circles and one of the intersection point.
		// RES/abc_triangle.png

		const tangentRelativeToC = Vector2D_Cart.sub(tangent, outer_circle.C);
		const a = outer_circle.r;
		const C_to_tangent = Vector2D_Cart.distBetween(outer_circle.C, tangent);
		const b = (a ** 2 - C_to_tangent ** 2) / (2 * C_to_tangent);
		const c = Math.sqrt(a ** 2 + b ** 2);



		const perpendicularCircleCenterRelativeToC = Vector2D_Cart.timesScalar(tangentRelativeToC, c / C_to_tangent);

		const absolutePerpendicularCircle = Vector2D_Cart.sum(outer_circle.C,
			perpendicularCircleCenterRelativeToC)

		return Circle_Cart.fromCenterAndRadius(absolutePerpendicularCircle, b);
	}

}

