import { Vector2D, Vector2D_Neutral } from "../../types";
import { Hexagon_Cart, Vector2D_Cart } from "../Math";


export class HexagonGridNode {
	public hexagon_cart: Hexagon_Cart = new Hexagon_Cart();

	public coords: Vector2D = Vector2D_Neutral();
	public grid: HexagonGrid = new HexagonGrid();

	// public static fromCartHexagon(hex: Hexagon_Cart): HexagonGridNode {
	// 	const new_hexagon_grid_node = new HexagonGridNode();
	// 	new_hexagon_grid_node.hexagon_cart = hex;
	// 	return new_hexagon_grid_node;
	// }


	public static createRoot(grid: HexagonGrid, hex: Hexagon_Cart): HexagonGridNode {
		const new_node = new HexagonGridNode();
		new_node.hexagon_cart = hex;
		new_node.grid = grid;
		new_node.coords = Vector2D_Neutral();
		return new_node;
	}

	public static fromNodeAndRelativeOffset(node: HexagonGridNode, offset: Vector2D, relative_coord: Vector2D): HexagonGridNode {
		const new_center = Vector2D_Cart.sum(
			node.hexagon_cart.center,
			offset
		)

		const new_cart_hexagon = Hexagon_Cart.fromCenterAndSide(
			new_center,
			node.hexagon_cart.side
		);

		const new_node = new HexagonGridNode();
		new_node.hexagon_cart = new_cart_hexagon;
		new_node.coords = Vector2D_Cart.sum(node.coords, relative_coord);
		new_node.grid = node.grid;
		return new_node;
	}

  public static solveForCoords(root: HexagonGridNode, coords: Vector2D): HexagonGridNode {

    const offset_from_root = {
      x: coords.x * root.hexagon_cart.side * 1.5,
      y: coords.y * root.hexagon_cart.side * Math.sqrt(3)
    };

    
    const new_center = Vector2D_Cart.sub(root.hexagon_cart.center, offset_from_root );
    // const new_center = Vector2D_Cart.sum({x: 0, y: 0 }, root.hexagon_cart.center );

		const new_cart_hexagon = Hexagon_Cart.fromCenterAndSide(
			// offset_from_root,
			// {x: 0, y: 0 },
      // root.hexagon_cart.center, 
      new_center, 
			root.hexagon_cart.side
		);

    const new_node = new HexagonGridNode();
		new_node.hexagon_cart = new_cart_hexagon;
		new_node.coords = coords;
		new_node.grid = root.grid;
    return new_node;
  }




	public solveNeighbours(): Vector2D[] {
		const solved: Vector2D[] = [];
		const neighboursCenters = this.hexagon_cart.getNeighboursCenters();
		const neighboursCoordinates = HexagonGrid.getNeighboursRelativeCoordinates();

		neighboursCoordinates.forEach((relative_coords: Vector2D, i: number) => {
			const new_coords = Vector2D_Cart.sum(relative_coords, this.coords);
			const relative_neighbour = this.grid?.getNode(new_coords);

			if (!relative_neighbour) {
				this.grid?.setNode(new_coords, HexagonGridNode.fromNodeAndRelativeOffset(
					this,
					neighboursCenters[i],
					relative_coords
				));
				solved.push(new_coords);
			}
		});

		return solved;
	}


	public getNeighboursAsList(): Array<HexagonGridNode> {
		const list: Array<HexagonGridNode> = [];


		const neighboursCoordinates = HexagonGrid.getNeighboursRelativeCoordinates()
			.map(relative_coords => Vector2D_Cart.sum(relative_coords, this.coords));

		for (let neighbour of neighboursCoordinates) {

			if (this.grid.hasNode(neighbour)) {
				list.push(this.grid.getNode(neighbour)!);
			}
		}

		return list;
	}
}

export class HexagonGrid {
	public root: HexagonGridNode | null = null;

	public nodes: Map<string, HexagonGridNode> = new Map();

	public static vector2DToHash(v: Vector2D): string {
		return `${v.x}:${v.y}`;
	}

	public static vector2DFromHash(v: string): Vector2D {
    const components = v.split(':');
    
    return {
      x: parseInt(components[0]) || 0,
      y: parseInt(components[1]) || 0
    }
	}

	public getNode(coords: Vector2D) {
		return this.nodes.get(
			HexagonGrid.vector2DToHash(coords)
		);
	};

	public hasNode(coords: Vector2D) {
		return this.nodes.has(
			HexagonGrid.vector2DToHash(coords)
		);
	};

	public setNode(coords: Vector2D, node: HexagonGridNode) {
		this.nodes.set(
			HexagonGrid.vector2DToHash(coords),
			node
		);
	};



	public static fromCenterHexagon(center: Hexagon_Cart) {
		const new_hexagon_grid = new HexagonGrid();
		new_hexagon_grid.root = HexagonGridNode.createRoot(new_hexagon_grid, center);
		new_hexagon_grid.setNode({ x: 0, y: 0 }, new_hexagon_grid.root);
		return new_hexagon_grid;
	}


	public static getNeighboursRelativeCoordinates(): Array<Vector2D> {
		return [
			// [0,  pi/3]
			{
				x: 1,
				y: 1
			},
			// [pi/3, 2pi/3] 
			{
				x: 0,
				y: 2
			},
			// [2pi/3, pi] 
			{
				x: -1,
				y: 1
			},
			// [pi, 4pi/3] 
			{
				x: -1,
				y: -1
			},
			// [4pi/3, 5pi/3] 
			{
				x: 0,
				y: -2
			},
			// [5pi/3, 0] 
			{
				x: 1,
				y: -1
			}
		];
	}



	public solveLevel1() {
		this.root?.solveNeighbours();
	}

	public solveLevel2() {
		this.root?.getNeighboursAsList().map(node => node.solveNeighbours());
	}


  // trapezoid bounds:
  // 0 ... 6
  // 50 ... 17


	public solveUntilOutOfBounds(offset = Vector2D_Neutral()) {
		// processing [ root]
		//  foreach in processing: 
		//    processing.solve neighbours
		//    filter out solved
		//    filter out of bounds
		// while unsolved.length  ... solve()


    const relative_root =  HexagonGridNode.solveForCoords(this.root!, offset);

    this.setNode(offset, relative_root);

		const solved_lookup_table: Record<string, boolean> = { [HexagonGrid.vector2DToHash(offset)]: true };

		let processing = offset ? [offset] : [];



		while (processing.length) {
			// all neigbours
			const currently_solved: Vector2D[] = [];
			const next_step_processing: Vector2D[] = [];


			processing.forEach((coords: Vector2D) => {
				const node = this.getNode(coords);
				currently_solved.push(
					...node?.solveNeighbours()!
				);
			});

			currently_solved.forEach(raw_node_coords => {
				const crt_hash = HexagonGrid.vector2DToHash(raw_node_coords);
				
        
        // const crt_node = this.getNode(raw_node_coords)!;
				// const crt_distance = Vector2D_Cart.magnitude(
				// 	crt_node.hexagon_cart.center
				// );

				// const in_bounds = crt_distance <= 20000 + crt_node.hexagon_cart.side;


        
        const in_bounds = raw_node_coords.x >( offset.x - 24) &&
          raw_node_coords.x <( offset.x + 24 )&& 
          raw_node_coords.y > offset.y &&
          raw_node_coords.y < (offset.y + 50); 


				const not_solved = !solved_lookup_table[crt_hash];

				if ( in_bounds && not_solved ) {
					next_step_processing.push(raw_node_coords);
					solved_lookup_table[crt_hash] = true;
				}
			});

			processing = next_step_processing;

		}
	}


  // TODO: returns object with array and coords
	public toHexagonCartArray(): Array<{
    hexagon_cart: Hexagon_Cart,
    coords: string
  }> {
		return Array.from(this.nodes.keys()).map(key => ({
      hexagon_cart: this.nodes.get(key)!.hexagon_cart,
      coords: key
    }));
	}

}