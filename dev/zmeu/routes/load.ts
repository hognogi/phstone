
import { Request, Response, NextFunction } from "express";
import fs from 'fs';
import path from 'path';
import {fileURLToPath} from 'url';

const __filename = fileURLToPath(import.meta.url);

// 👇️ "/home/john/Desktop/javascript"
const __dirname = path.dirname(__filename);


export default {
  route: '/load',
  handle: async (req: Request, res: Response, next: NextFunction) => {


    const {mount} = req.body;

    const {cellMap} = await import(__dirname + `/../../../WB/${mount}/cellMap.ts`);


    res.json({success: true, cellMap});
    next();
  }
}