
import { Request, Response, NextFunction } from "express";
import fs from 'fs';
import path from 'path';
import {fileURLToPath} from 'url';

const __filename = fileURLToPath(import.meta.url);

// 👇️ "/home/john/Desktop/javascript"
const __dirname = path.dirname(__filename);

console.log(__dirname);

export default {
  route: '/save',
  handle:(req: Request, res: Response, next: NextFunction) => {


    const {cellMap, mount} = req.body;

    // console.log('req.body', req.body);

    fs.writeFileSync(__dirname + `/../../../WB/${mount}/cellMap.ts`, `
    
      export const cellMap = ${JSON.stringify(cellMap, null, 4)};
    
    `);

    res.json({success: 'true'});
    next();
  }
}