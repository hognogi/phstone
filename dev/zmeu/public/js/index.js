

const delay = ms => new Promise(res => setTimeout(res, ms));
//Add event from js the keep the marup clean
function init() {
    document.querySelectorAll('.overlay-opener').forEach(el => {
        el.addEventListener('click', (e) => {
            const selector_to_open = e.target.getAttribute('data-target');
            show(selector_to_open);
        });
    });
    document.querySelector("#body-overlay").addEventListener("click", hideOverlay);
}

//The actual fuction
function show(id) {
    document.querySelector(id).classList.add('open');
    document.body.classList.add('overlay-open');
}


function hideOverlay() {
    document.body.classList.remove('overlay-open');
    document.querySelector('.overlay.open')?.classList.remove('open');
};

//Prevent the function to run before the document is loaded
document.addEventListener('readystatechange', function () {
    if (document.readyState === "complete") {
        init();
    }
});



document.querySelector('#form-generate').addEventListener('submit', (event) => {
    event.preventDefault();
    const form = event.target;

    const data = new FormData(form);

    const leftOffset = parseInt(data.get('width-start'));
    const width = parseInt(data.get('width-end')) - parseInt(data.get('width-start'));
    const length = parseInt(data.get('length'));

    const crt_track_cell = data.get('origin').split(':').map(x => parseInt(x));

    const trackDOM = document.querySelector('.track')

    trackDOM.setAttribute('data-origin', data.get('origin'));



    [...Array(length).keys()].forEach(async (i) => {


        const rowHTML = '<div class="flex hex-row">' + [...Array(width).keys()].map(j => {

            const x_coord = (j + leftOffset) * 2 + i % 2;
            const coords = [x_coord, i];

            const is_track = coords[0] === crt_track_cell[0];
            /*
                 <svg width="50px" height="50px" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6.32677 2.77363C6.43395 2.58799 6.63202 2.47363 6.84638 2.47363L17.1536 2.47363C17.3679 2.47363 17.566 2.58799 17.6732 2.77363L22.8268 11.6999C22.9339 11.8856 22.9339 12.1143 22.8268 12.2999L17.6732 21.2262C17.566 21.4118 17.3679 21.5262 17.1536 21.5262L6.84638 21.5262C6.63202 21.5262 6.43395 21.4118 6.32677 21.2262L1.17318 12.2999C1.066 12.1143 1.066 11.8856 1.17318 11.6999L6.32677 2.77363Z" stroke="${is_track ? '#600000' : '#000000'}" stroke-width=".5" stroke-linecap="round" stroke-linejoin="round"></path> 
                        </svg>
            */
            return `
          <div class="hex-cell" data-coords="${coords.join(':')}">
          </div>
        `;

        }).join('') + '</div>';

        trackDOM.insertAdjacentHTML('beforeend', rowHTML);
        console.log(`hadnled row...${i}`);
        await delay(20);
    });


    // document.querySelector('.track').innerHTML = hexagonGrid;
    hideOverlay();
});



document.querySelector('#form-populate').addEventListener('submit', async (event) => {
    event.preventDefault();
    const form = event.target;
    const data = new FormData(form);

    const trackDOM = document.querySelector('.track');

    const colors = data.get('colors').split(',');
    const allTypes = Array.from(colors.keys());

    const [x, y] = trackDOM.getAttribute('data-origin').split(':');

    const to_process = [{ x, y }];
    const adjacentOffset = [
        { x: 1, y: -1 }, // Rad = 0
        { x: 0, y: -2 }, // Rad = π/3
        { x: -1, y: - 1 }, // Rad = 2π/3
        { x: -1, y: 1 }, // Rad = π
        { x: 0, y: -2 }, // Rad = 4π/3
        { x: + 1, y: + 1 }, // Rad = 5π/3
    ]

    let processedCount = 0;


    do {

        const crt = to_process.pop();
        const crtDOM = document.querySelector(`[data-coords="${crt.x}:${crt.y}"]`);

        if (!crtDOM) continue;


        const adjacentDOM = adjacentOffset.map(offset => ({
            x: parseInt(crt.x) + offset.x,
            y: parseInt(crt.y) + offset.y
        })).map(adjCoords => (
            document.querySelector(`[data-coords="${adjCoords.x}:${adjCoords.y}"]`)
        )).filter(x => !!x);

        const adjTypes = adjacentDOM
            .map(adj => adj.getAttribute('data-type'))
            .filter(x => !!x)
            .map(parseInt);

        const possibleTypes = allTypes.filter(type => !adjTypes.includes(type));

        const randType = possibleTypes.length
            ? possibleTypes[Math.floor(Math.random() * possibleTypes.length)]
            : allTypes[Math.floor(Math.random() * allTypes.length)]

        crtDOM.setAttribute(`data-type`, randType);
        processedCount++;
        console.log(`processed ${processedCount}: ${crt.x}:${crt.y}`)

        to_process.push(
            ...adjacentDOM.filter(adj => !adj.getAttribute(`data-type`)) // data-type='0' falsey possible falsey evaluation
                .map(adj => {
                    const [x, y] = adj.getAttribute('data-coords').split(':');
                    return { x, y };
                })
        );

    } while (to_process.length);



});

document.querySelector('#build_track').addEventListener('click', (evt) => {
    evt.preventDefault();

    document.querySelectorAll('[data-elapsed]').forEach(el => { el.removeAttribute('data-elapsed') });

    let crt = document
        .querySelector('.track')
        .getAttribute('data-origin')
        .split(':')
        .map(x => parseInt(x));

    let crtDOM = document.querySelector(`[data-coords="${crt.join(':')}"]`);


    // const next = [crt[0], crt[1] + 2];
    // const nextDOM = document.querySelector(`[data-coords="${next.join(':')}"]`);

    let elapsed = [0, 0];
    let next;

    while (crtDOM) {

        crtDOM.setAttribute('data-elapsed', elapsed.join(':'));
        const step = crtDOM.getAttribute('data-step') ? crtDOM.getAttribute('data-step').split(':').map(x => parseInt(x)) : [0, 2];
        const nextFromDom = crtDOM.getAttribute('data-next') ? crtDOM.getAttribute('data-next').split(':').map(x => parseInt(x)) : null;
        
        
        
        next =  nextFromDom ? nextFromDom : [crt[0] + step[0], crt[1] + step[1]];


        crtDOM.setAttribute('data-next', next.join(':'));

        elapsed = [elapsed[0] + step[0], elapsed[1] + step[1]];


        //next
        crt = next;
        crtDOM = document.querySelector(`[data-coords="${crt.join(':')}"]`);
    }


});


    // start from origin



    // if no neighbours random

    // if neighbours, random out of left pieces.

document.body.addEventListener('click', (evt) => {


    let is_cell = false;
    let walker = evt.target;

    do {
        if (walker.classList.contains('hex-cell')) {
            is_cell = true;
            break;
        }
        walker = walker.parentNode;
    } while (!!walker.parentNode && !!walker.parentNode.classList);


    if (is_cell) {

        const cellDOM = walker;
        hideOverlay();

        document.querySelector('#form-cell-editor [name="coords"]').value = cellDOM.getAttribute('data-coords');
        document.querySelector('#form-cell-editor [name="type"]').value = cellDOM.getAttribute('data-type');
        document.querySelector('#form-cell-editor [name="elapsed"]').value = cellDOM.getAttribute('data-elapsed');
        document.querySelector('#form-cell-editor [name="next"]').value = cellDOM.getAttribute('data-next');
        document.querySelector('#form-cell-editor [name="step"]').value = cellDOM.getAttribute('data-step') || '0:2';


        show('#cell-editor');

    }
});



document.querySelector('#form-cell-editor').addEventListener('submit', (evt) => {
    evt.preventDefault();
    evt.stopPropagation();
    const form = evt.target;

    const data = new FormData(form);

    const cell = document.querySelector(`[data-coords="${data.get('coords')}"]`);

    cell.setAttribute('data-type', data.get('type'));
    // cell.setAttribute('data-elapsed', data.get('elapsed'));
    // cell.setAttribute('data-next', data.get('next'));
    cell.setAttribute('data-step', data.get('step'));

    hideOverlay();
});

document.querySelector('#form-saveload').addEventListener('submit', async (evt) => {
    evt.preventDefault();
    evt.stopPropagation();

    const form = evt.target;

    const data = new FormData(form);
    // serialize cells

    const isSave = evt.submitter.value === 'Save';

    if (isSave && !confirm("Are you sure you want to save?")) {
        return;
    }

    const nodes = Array.from(document.querySelectorAll('.hex-cell'));

    const cellMap = isSave ? nodes.reduce((acc, crt) => {
        const coords = crt.getAttribute('data-coords');
        const type = crt.getAttribute('data-type');
        const elapsed = crt.getAttribute('data-elapsed');
        const next = crt.getAttribute('data-next');

        acc[coords] = { coords, type, elapsed, next };
        return acc;
    }, {}) : null;


    const response = await fetch(isSave ? '/save' : '/load', {
        method: "post",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            ...(cellMap ? { cellMap } : {}),
            mount: data.get('mount')
        })
    });
    const result = await response.json();

    if (!result.success) {
        alert('problemo');
    }
    // fetch a post request to the path /save

    if (isSave) return;

    const hexagonGrid = '<div class="flex hex-row">' + Object.values(result.cellMap).map((c, i, arr) => {
        const coords = `data-coords="${c.coords}"`;
        const type = `data-type="${c.type}"`;
        const elapsed = c.elapsed ? `data-elapsed="${c.elapsed}"` : '';
        const next = c.next ? `data-next="${c.next}"` : '';

        const parsedElapsed = c.elapsed ?  c.elapsed.split(':').map(n => parseInt(n)) : null;
        const parsedNext = c.next ?  c.next.split(':').map(n => parseInt(n)) : null;

        const step = (parsedNext && parsedElapsed) ? `data-step="` +[ parsedNext[0] - parsedElapsed[0], parsedNext[1] - parsedElapsed[1]  ].join(':') + `"` : '';

        const cell = `<div class="hex-cell" ${coords}  ${type}  ${elapsed} ${step} ${next} ></div>`;
        const y_coord = c.coords.split(':')[1];
        if (arr[i + 1] && arr[i + 1].coords.split(':')[1] !== y_coord) {
            return cell + '</div> <div class="flex hex-row">';
        }
        return cell;
    }).join('') + '</div>';


    document.querySelector('.track').innerHTML = hexagonGrid;








});