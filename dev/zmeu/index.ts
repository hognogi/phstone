import express from 'express';
const app = express();
const port = 3000;
app.use(express.json({limit: '5mb'}));

import save_route from './routes/save';
import load_route from './routes/load';

app.use(express.static('dev/zmeu/public'))

app.use(save_route.route, save_route.handle);
app.use(load_route.route, load_route.handle);


app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});